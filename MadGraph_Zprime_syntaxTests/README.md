Histo scripts for ExRootAnalysis converted LHE files
====================================================
  
Compare the following syntaxes :  
a. `p p > y1, y1 > e+ e- && bwcutoff = 15`  
b. `p p > y1, y1 > e+ e- && bwcutoff = 15000`  
c. `p p > y1 > e+ e- && bwcutoff = 15`  
d. `p p > y1 > e+ e- && bwcutoff = 15000`  
  
```sh
python histZprime.py inputROOT/pptoy1toee_BW15000.root
python plotter.py --inputDir ./inputHIST/ --histConfig test.dat --histStyleConfig style.dat
```
  
**Results**
b. c. d. have exactly same distributions and bwcutoff value is not recommended to be too big (> 20). Possible to conclude that inserting `> y1 >` yields ill behavior just like setting bwcutoff = 15000.

**To do**
Histo styling codes need to be done.

01/AUG/2021
