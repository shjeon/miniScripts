import sys
import ROOT

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)

try:
    inputFile = sys.argv[1]
except:
    print ("[Error] No input file : python {0} <XXX.root>".format(sys.argv[0]))

openFile = ROOT.TFile.Open(inputFile)
histFile = ROOT.TFile("inputHIST/hist_{0}".format(inputFile.split("/")[1]), "RECREATE")

myTree = openFile.Get("LHEF")

histObjects = []
histNames = ["massll", "pTll", "pTl"];
histmll = ROOT.TH1D("mll","mll",5000,0.,5000.)
histptll = ROOT.TH1D("ptll","ptll",100,0.,100.)
histptl = ROOT.TH1D("ptl","ptl",5000,0.,5000.)

for iEvent in range(0, myTree.GetEntries()):

    myTree.GetEntry(iEvent)
    if(iEvent == 0):
        weight = (myTree.GetLeaf("Event.Weight").GetValue(0)/myTree.GetEntries())

    if(iEvent < 10000):
        if(iEvent%1000==0): print "Running over ME event "+str(iEvent)+"/"+str(myTree.GetEntries())
    else:
        if(iEvent%5000==0): print "Running over ME event "+str(iEvent)+"/"+str(myTree.GetEntries())

    for i in range(0,int(myTree.GetLeaf("Particle_size").GetValue())):
        if (myTree.GetLeaf("Particle.PID").GetValue(i) == 11):
            iEle = i
        if (myTree.GetLeaf("Particle.PID").GetValue(i) == -11):
            iPos = i

    Ele = ROOT.TLorentzVector(); Pos = ROOT.TLorentzVector();
    Ele.SetPxPyPzE(myTree.GetLeaf("Particle.Px").GetValue(iEle),myTree.GetLeaf("Particle.Py").GetValue(iEle),myTree.GetLeaf("Particle.Pz").GetValue(iEle),myTree.GetLeaf("Particle.E").GetValue(iEle))
    Pos.SetPxPyPzE(myTree.GetLeaf("Particle.Px").GetValue(iPos),myTree.GetLeaf("Particle.Py").GetValue(iPos),myTree.GetLeaf("Particle.Pz").GetValue(iPos),myTree.GetLeaf("Particle.E").GetValue(iPos))

    llSystem = ROOT.TLorentzVector()
    llSystem = Ele + Pos

    histmll.Fill(llSystem.M(), weight)
    histptll.Fill(llSystem.Pt(), weight)
    histptl.Fill(Ele.Pt(), weight)
    histptl.Fill(Pos.Pt(), weight)

histObjects.append(histmll)
histObjects.append(histptll)
histObjects.append(histptl)

#hist_pptoee.root  hist_pptoy1_y1toee.root  hist_pptoy1_y1toee_BW15000.root  hist_pptoy1toee.root  hist_pptoy1toee_BW15000.root
histColors = [["pptoee", ROOT.kRed], ["pptoy1_y1toee", ROOT.kBlue], ["pptoy1_y1toee_BW15000", ROOT.kGreen], ["pptoy1toee", ROOT.kOrange], ["pptoy1toee_BW15000",ROOT.kBlack]]
markerStyles = [["pptoee", 20], ["pptoy1_y1toee", 26], ["pptoy1_y1toee_BW15000", 21], ["pptoy1toee", 32], ["pptoy1toee_BW15000", 24]]
lineStyles = [["pptoee", 1], ["pptoy1_y1toee", 3], ["pptoy1_y1toee_BW15000", 5], ["pptoy1toee", 7], ["pptoy1toee_BW15000", 9]]

for iHist in range(len(histNames)):

    this_name = inputFile.split("/")[1].replace(".root","")
    for jHist in range(0,len(histColors)):
        print iHist
        if this_name == histColors[jHist][0]:
            histObjects[iHist].SetLineColor(histColors[jHist][1])
            histObjects[iHist].SetMarkerColor(histColors[jHist][1])
            histObjects[iHist].SetMarkerStyle(markerStyles[jHist][1])
            histObjects[iHist].SetLineStyle(lineStyles[jHist][1])
            histObjects[iHist].SetLineWidth(2)

#    histObjects[iHist].SetLineColor(ROOT.kRed)

histFile.Write()
