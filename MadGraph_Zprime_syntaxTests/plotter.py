import os
import sys
import ROOT
import argparse

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)
ROOT.TH1.SetDefaultSumw2(True)

def get_argument():

    parser = argparse.ArgumentParser(description='ROOT plotter')
    parser.add_argument('--inputDir', dest='inputDir', help='Histogram ROOT file directory')
    parser.add_argument('--histConfig', dest='histConfig', help='Histogram configuration file')
    parser.add_argument('--histRef', dest='histRef', help='ROOT file that will be used as reference')
#    parser.add_argument('--histStyleConfig', dest='histStyleConfig', help='Histogram style setting file')

    args = parser.parse_args()
    if args.inputDir == None:
        print ("[Error] No input directory : --inputDir <XXX>")
        sys.exit(1)
    elif args.histConfig == None:
        print ("[Error] No histogram configuration : --histConfig <XXX.dat>")
        sys.exit(1)

    return args

def check_inputDir(inputDir, histRef):

    raw_inputList = os.listdir(inputDir)
    histFiles = []

    for this in raw_inputList:

        if this == histRef:
            continue

        if this.endswith(".root") and this.startswith("hist_"):
            histFiles.append(ROOT.TFile("{0}/{1}".format(inputDir, this)))

    if not (histRef is None):
        if not ((histRef.endswith(".root") and histRef.startswith("hist_"))):
            print histRef
            print ("[Error] Reference histogram should be named <hist_XXX.root>")
            sys.exit(1)

        histRefFile = ROOT.TFile(histRef)

    else:
        histRefFile = None

    return histFiles, histRefFile

def check_histConfig(histConfig):

    lines_histConfig = open(histConfig).readlines()
    histConfigs = []

    for this in lines_histConfig:
        if "#" in this:
            continue
        histConfig = this.replace(" ","").replace("\t","").replace("\\"," ").split(",")

        try:
            histName = histConfig[0]
            histXRebin = histConfig[1]
            histXMin = histConfig[2]
            histXMax = histConfig[3]
            histYMin = histConfig[4]
            histYMax = histConfig[5]
            histXLabel = histConfig[6]
            histYLabel = histConfig[7]
        except:
            print ("[Error] Histogram configuration is missing")
            sys.exit(1)
        histConfigs.append([histName, histXRebin, histXMin, histXMax, histYMin, histYMax, histXLabel, histXLabel])

    return histConfigs

def check_histStyleConfig(histStyleConfig):

    lines_histStyleConfig = open(histStyleConfig).readlines()
    histStyleConfigs = []

    for this in lines_histStyleConfig:
        if "#" in this:
            continue
        histStyleConfig = this.replace(" ","").replace("\t","").replace("\\"," ").split(",")

        try:
            histName = histStyleConfig[0]
            histColor = histStyleConfig[1]
            histLineStyle = histStyleConfig[2]
            histMarkerStyle = histStyleConfig[3]
        except:
            print ("[Error] Histogram configuration is missing")
            sys.exit(1)
        histStyleConfigs.append([histName, histColor, histLineStyle, histMarkerStyle])

    return histStyleConfigs

def draw_histograms(histFiles, histConfigs, histStyleConfigs, histRefFile):

    for histConfig in histConfigs:

        canvas, padup, paddown = set_canvas(histFiles,histRefFile)

        legend = ROOT.TLegend(0.2, 0.4, 0.5, 0.8)

        for histFile in histFiles:
            padup.cd()

            this_hist = histFile.Get(histConfig[0])
#            this_hist = set_colorStyle(this_hist, histConfig[0], histStyleConfigs)
            this_hist.Rebin(int(histConfig[1]))
            this_hist.Draw("histsame")

            if not (histRefFile is None):
                paddown.cd()
                histRatio = histRefFile.Get(histConfig[0]).Clone("histRatio")
                histRatio.Divide(histFile.Get(histConfig[0]))
                histRatio.Draw("EPsame")

        padup.SetLogy()
        canvas.SaveAs("outputHIST/{0}.pdf".format(histConfig[0]))
#            this = this.Rebin(int(histConfig[1]))
#            this.GetXaxis().SetRangeUser(float(histConfig[2]), float(histConfig[3]))
#            this.GetYaxis().SetRangeUser(float(histConfig[4]), float(histConfig[5]))
#            this.GetXaxis().SetLabel(histConfig[6])
#            this.GetYaxis().SetLabel(histConfig[7])


def set_canvas(histFiles,histRefFile):

    canvas = ROOT.TCanvas("canvas", "canvas", 1000, 1000)

    if not (histRefFile is None):
        padup = ROOT.TPad("padup", "padup", 0.02, 0.02, 0.98, 0.66)
        paddown = ROOT.TPad("paddown", "paddown", 0.02, 0.66, 0.98, 0.98)
        padup.Draw()
        paddown.Draw()

    else:
        padup = ROOT.TPad("padup", "padup", 0.02, 0.02, 0.98, 0.98)
        padup.Draw()
        paddown = None

    return canvas, padup, paddown

def set_colorStyle(this_hist, histName, histStyleConfigs):

    for histStyleConfig in histStyleConfigs:
        print histStyleConfig[0], histName
        if (histStyleConfig[0] == histName):
            print (histStyleConfig[1])
            this_hist.SetLineColor(histStyleConfig[1])
#            histColor = histStyleConfig[1]
#            histLineStyle = histStyleConfig[2]
#            histMarkerStyle = histStyleConfig[3]
    return this_hist

def main():

    args = get_argument()
    histFiles, histRefFile = check_inputDir(args.inputDir, args.histRef)
    histConfigs = check_histConfig(args.histConfig)
#    histStyleConfigs = check_histStyleConfig(args.histStyleConfig)
    draw_histograms(histFiles, histConfigs, histStyleConfigs, histRefFile)

if __name__ == "__main__":
	main()

